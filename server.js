require('dotenv').config();

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const uri = `mongodb+srv://admin:${process.env.mongoPass}${process.env.mongoAtlasURL}`;
mongoose.connect(uri, {
    dbName: 'warehouse'
});
const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.once('open', () => console.log('Connected to database'));

const productsRouter = require('./routes/products');
app.use('/products', productsRouter);

app.listen(3000, () => console.log('server started on port 3000'));

module.exports = app