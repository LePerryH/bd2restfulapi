require('dotenv').config();

const express = require('express');
const router = express.Router();
const Product = require('../models/product');
const bodyParser = require('body-parser');

//id for a shovel
//6550eee62ce42849cb59d3d3

router.use(bodyParser.json());

//wszystkie produkty

// router.get('/', async (req, res) => {
//     try {
//         const products = await Product.find()
//         res.json(products)
//     } catch (err) {
//         res.status(500).json({ message: err.message })
//     }
// })

router.get('/', async (req, res) => {
  try {
    const { sort, search } = req.query;
    let findQuery = {};
    let sortQuery = {};
    if (search) {
      if (search == "available") {
        findQuery = { amount: { $gt: 0 } };
      } else if (search == "unavailable") {
        findQuery = { amount: { $eq: 0 } };
      } else {
        findQuery = {
          name: { $regex: search, $options: "i" },
        };
      }
    }
    if (sort) {
      if (sort === "name") {
        sortQuery = { name: 1 };
      }
      if (sort === "price") {
        sortQuery = { price: 1 };
      }
      if (sort === "amount") {
        sortQuery = { amount: 1 };
      }
    }

    const products = await Product.find(findQuery).sort(sortQuery);
    res.status(200).json(products);
  } catch (error) {
    console.error("Error:", error.message);
    res.status(500).send("Internal Server Error");
  }
});

//report 

router.get('/report', async (req, res) => {
    try {
        const productsArray = await Product.find()
        //console.log(productsArray)
        const report = productsArray.reduce((acc, curr) => {
            acc.push({
                name: curr.name,
                amount: curr.amount,
                total_value: curr.amount*curr.price
            })
            return acc;
        }, [])
        res.json(report)
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

router.post('/', async (req, res) => {
    const product = new Product({
        name: req.body.name,
        price: req.body.price,
        description: req.body. description,
        amount: req.body.amount,
        measure: req.body.measure
    });
    const productsArray = await Product.find();
    const checkForName = productsArray.some(entry => entry.name === product.name)
    console.log(checkForName)
    try {
        if (checkForName === false) {
        const newProduct = await product.save();
        res.status(201).json(newProduct);
        } else {
            res.status(400).json({message: "name is not unique"})
        }
    } catch (err) {
        res.status(400).json({message: err.message});
    }
})

router.get('/:id', getProduct, (req, res) => {
    res.json(res.product)
  })

router.put('/:id', getProduct, async(req, res) => {
    if (req.body.name != null) {
        res.product.name = req.body.name
    }
    if (req.body.price != null) {
        res.product.price = req.body.price
    }
    if (req.body.description != null) {
        res.product.description = req.body.description
    }
    if (req.body.amount != null) {
        res.product.amount = req.body.amount
    }
    if (req.body.measure != null) {
        res.product.measure = req.body.measure
    }
    try {
        const updatedProduct = await res.product.save();
        res.json(updatedProduct)
    } catch (err) {
        res.status(400).json({message: err.message})
    }
})

router.delete('/:id', getProduct, async (req, res) => {
    try {
      await res.product.deleteOne();
      res.json({ message: 'Deleted Entry' })
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })

async function getProduct(req, res, next) {
    let product;
    try {
        product = await Product.findById(req.params.id)
        if (product == null) {
        return res.status(404).json({ message: 'Cannot find product' })
        }
    } catch (err) {
        return res.status(500).json({ message: err.message })
    }

    res.product = product
    next()
    }

module.exports = router;