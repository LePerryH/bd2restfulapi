const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    description: {
        type: String
    },
    amount: {
        type: Number,
        required: true,
        default: 0
    },
    measure: {
        type: String,
        required: true
    }
},
{
    collection: 'products'
});

module.exports = mongoose.model('Product', productSchema);